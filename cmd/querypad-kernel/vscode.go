package main

import "gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/proto"

func (s *session) UseVSCodeAPI() {
	type promptOptions struct {
		Prompt      string
		Placeholder string
		Secret      bool
	}

	s.interpreter.Package("vscode").
		Add("PromptOptions", (*promptOptions)(nil)).
		Add("Prompt", func(opts *promptOptions) (string, bool) {
			if opts == nil {
				opts = new(promptOptions)
			}

			var msg proto.FromKernel
			msg.Prompt(opts.Prompt, opts.Placeholder, opts.Secret)
			s.stream.Send(&msg)

			select {
			case <-s.stream.Context().Done():
				return "", false
			case resp := <-s.prompted:
				if resp.Value == nil {
					return "", false
				}
				return *resp.Value, true
			}
		}).
		Add("Cache", func(key string, get func() string) string {
			var msg proto.FromKernel
			msg.ReadCache(key)
			s.stream.Send(&msg)

			var cached *proto.ToKernel_Cached
			select {
			case <-s.stream.Context().Done():
				return ""
			case cached = <-s.cached:
			}

			if cached.Value != nil {
				return *cached.Value
			}

			v := get()
			msg.WriteCache(key, v)
			s.stream.Send(&msg)

			select {
			case <-s.stream.Context().Done():
			case <-s.cached:
			}

			return v
		}).
		Add("Uncache", func(key string) {
			var msg proto.FromKernel
			msg.WriteCache(key, "")
			s.stream.Send(&msg)

			select {
			case <-s.stream.Context().Done():
			case <-s.cached:
			}
		}).
		Export()
}
