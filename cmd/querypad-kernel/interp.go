package main

import (
	"errors"
	"fmt"
	"go/scanner"

	"github.com/traefik/yaegi/interp"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/interpreter"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/proto"
)

func convertInterpreterError(res *proto.FromKernel_Executed, err error) {
	var errPanic interp.Panic
	if errors.As(err, &errPanic) {
		if halt, ok := errPanic.Value.(notebookHalt); ok {
			res.AddError(fmt.Sprintf("Halted: %v", halt.value), "")
			return
		}
		res.AddError(fmt.Sprint(errPanic.Value), string(errPanic.Stack))
		return
	}

	var errList scanner.ErrorList
	if errors.As(err, &errList) {
		for _, err := range errList {
			var errScanner *scanner.Error
			if errors.As(err, &errScanner) {
				res.AddErrorAt(err.Msg, "", errScanner.Pos.Line, errScanner.Pos.Line)
			} else {
				res.AddError(err.Msg, "")
			}
		}
		return
	}

	if errors.Is(err, interpreter.ErrCanceled) {
		res.AddError("Canceled", "")
		return
	}

	res.AddError(err.Error(), "")
}
