package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	"github.com/traefik/yaegi/stdlib"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/interpreter"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/proto"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/sqldata"
)

type kernel struct {
	proto.UnsafeKernelServer
}

type session struct {
	stream      proto.Kernel_SessionServer
	interpreter *interpreter.Session

	setup    *proto.ToKernel_Setup
	cached   chan *proto.ToKernel_Cached
	prompted chan *proto.ToKernel_Prompted

	mu          sync.Mutex
	connections map[string]*connection
}

type connection struct {
	id, provider string
	db           *sql.DB
}

func (k *kernel) Session(stream proto.Kernel_SessionServer) error {
	msg, err := stream.Recv()
	if err != nil {
		return err
	}

	setup, ok := msg.Get().(*proto.ToKernel_Setup)
	if !ok {
		return errors.New("did not receive setup message")
	}

	s := &session{
		stream:      stream,
		interpreter: interpreter.NewSession(stream.Context()),
		setup:       setup,
		cached:      make(chan *proto.ToKernel_Cached),
		prompted:    make(chan *proto.ToKernel_Prompted),
		connections: map[string]*connection{},
	}

	s.interpreter.Use(stdlib.Symbols)
	s.UseNotebookAPI()
	s.UseVSCodeAPI()

	evalSem := make(chan struct{}, 1)

	var cancel context.CancelFunc = func() {}
	for {
		msg, err := stream.Recv()
		if err != nil {
			return err
		}

		switch v := msg.Get().(type) {
		case *proto.ToKernel_ExecuteCode:
			var ctx context.Context
			ctx, cancel = context.WithCancel(stream.Context())

			evalSem <- struct{}{}
			go func() {
				defer cancel()
				defer func() { <-evalSem }()

				var result proto.FromKernel_Executed
				defer stream.Send(&proto.FromKernel{Kind: &proto.FromKernel_Executed_{Executed: &result}})

				_, d, err := s.interpreter.Eval(ctx, v.Code, interpreter.EvalOptions{
					OnStdout: func(b []byte) { s.SendOutput(&proto.Content{Type: "stdout", Value: b}) },
					OnStderr: func(b []byte) { s.SendOutput(&proto.Content{Type: "stderr", Value: b}) },
					OnPanic:  func(v interface{}) { result.AddError(fmt.Sprint(v), string(debug.Stack())) },
				})
				result.SetDuration(d)

				if err != nil {
					convertInterpreterError(&result, err)
				}
			}()

		case *proto.ToKernel_ExecuteQuery:
			s.mu.Lock()
			conn, ok := s.connections[v.Connection]
			s.mu.Unlock()

			var result proto.FromKernel_Executed
			if !ok {
				result.AddError("Unknown connection", "")
				stream.Send(&proto.FromKernel{Kind: &proto.FromKernel_Executed_{Executed: &result}})
				break
			}

			var ctx context.Context
			ctx, cancel = context.WithCancel(stream.Context())

			evalSem <- struct{}{}
			go func() {
				defer cancel()
				defer func() { <-evalSem }()

				defer stream.Send(&proto.FromKernel{Kind: &proto.FromKernel_Executed_{Executed: &result}})

				before := time.Now()
				rows, err := conn.db.QueryContext(ctx, v.Query)
				after := time.Now()
				if err != nil {
					result.AddError(err.Error(), "")
					return
				}

				result.SetDuration(after.Sub(before))

				for ok := true; ok; ok = rows.NextResultSet() {
					cols, data, err := sqldata.GetRows(rows)
					if err != nil {
						result.AddError(err.Error(), "")
						continue
					}

					if len(data) == 0 {
						continue
					}

					s.SendOutput(
						&proto.Content{Type: "text/csv", Value: sqldata.MarshalCSV(cols, data)},
						&proto.Content{Type: "text/html", Value: sqldata.MarshalHTML(cols, data)},
						&proto.Content{Type: "application/json", Value: sqldata.MarshalJSON(cols, data)},
					)
				}
			}()

		case *proto.ToKernel_CancelExecute:
			cancel()

		case *proto.ToKernel_Cached:
			s.cached <- v

		case *proto.ToKernel_Prompted:
			s.prompted <- v

		case *proto.ToKernel_ListConnections:
			s.mu.Lock()
			conns := s.protoConns()
			s.mu.Unlock()

			var msg proto.FromKernel
			msg.Connections(conns)
			stream.Send(&msg)
		}
	}
}

type protoConn = proto.FromKernel_Connections_Connection

func (s *session) protoConns() []*protoConn {
	var conns = make([]*protoConn, 0, len(s.connections))
	for _, conn := range s.connections {
		conns = append(conns, &protoConn{
			Id:       conn.id,
			Provider: proto.String(conn.provider),
		})
	}
	return conns
}

func (s *session) addConn(conn *connection) *connection {
	old := s.connections[conn.id]
	s.connections[conn.id] = conn
	return old
}
