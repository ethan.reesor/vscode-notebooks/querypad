package main

import (
	"context"
	"database/sql"
	"path/filepath"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/dolt"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/filedb"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/proto"
)

type notebookHalt struct {
	value interface{}
}

type stringContent struct {
	Type, Value string
}

func (s *stringContent) toContent() []*proto.Content {
	return []*proto.Content{
		{Type: s.Type, Value: []byte(s.Value)},
	}
}

type bytesContent struct {
	Type  string
	Value []byte
}

func (s *bytesContent) toContent() []*proto.Content {
	return []*proto.Content{
		{Type: s.Type, Value: s.Value},
	}
}

func (s *session) SendOutput(content ...*proto.Content) error {
	var msg proto.FromKernel
	msg.Output(content...)
	return s.stream.Send(&msg)
}

func (s *session) UseNotebookAPI() {
	type content interface {
		toContent() []*proto.Content
	}

	s.interpreter.Package("notebook").
		Add("Path", s.setup.Path).
		Add("Dir", filepath.Dir(s.setup.Path)).
		Add("Halt", func(v interface{}) {
			panic(notebookHalt{v})
		}).
		Add("Inspect", func(v ...interface{}) {
			for _, v := range v {
				s.SendOutput(&proto.Content{
					Type:  "text/plain",
					Value: []byte(spew.Sdump(v)),
				})
			}
		}).
		Add("Show", func(v ...content) {
			for _, v := range v {
				s.SendOutput(v.toContent()...)
			}
		}).
		Add("StringContent", (*stringContent)(nil)).
		Add("BytesContent", (*bytesContent)(nil)).
		Add("Connect", s.notebookConnect).
		Add("MustConnect", s.notebookMustConnect).
		Export()

	s.interpreter.Package("notebook/filedb").
		Add("CSV", filedb.CSV).
		Add("Attach", filedb.Attach).
		Add("Detach", filedb.Detach).
		Add("GetCatalog", filedb.GetCatalog).
		Add("NewCatalog", filedb.NewCatalog).
		Add("NewDatabase", filedb.NewDatabase).
		Add("Load", s.fileDBLoad).
		Add("MustLoad", s.fileDBMustLoad).
		Export()

	s.interpreter.Package("notebook/dolt").
		Add("Attach", dolt.Attach).
		Add("Detach", dolt.Detach).
		Add("GetCatalog", dolt.GetCatalog).
		Add("NewCatalog", dolt.NewCatalog).
		Add("LoadFrom", s.doltLoadFrom).
		Add("MustLoadFrom", s.doltMustLoadFrom).
		Export()
}

func (s *session) resolveRel(path string) string {
	if filepath.IsAbs(path) {
		return path
	}

	return filepath.Join(filepath.Dir(s.setup.Path), path)
}

func (s *session) notebookConnect(id, driver, dsn string) (*sql.DB, error) {
	db, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}

	s.mu.Lock()
	old := s.addConn(&connection{id, driver, db})
	conns := s.protoConns()
	s.mu.Unlock()

	if old != nil {
		old.db.Close()
	}

	var msg proto.FromKernel
	msg.Connections(conns)
	s.stream.Send(&msg)

	return db, nil
}

func (s *session) notebookMustConnect(id, driver, dsn string) *sql.DB {
	db, err := s.notebookConnect(id, driver, dsn)
	if err != nil {
		panic(notebookHalt{err})
	}
	return db
}

func (s *session) fileDBLoad(catalog, database, file string, loader filedb.TableLoader) error {
	file = s.resolveRel(file)

	_, err := filedb.
		GetCatalog(catalog, true).
		GetDatabase(database, true).
		Load(file, loader)
	return err
}

func (s *session) fileDBMustLoad(catalog, database, file string, loader filedb.TableLoader) {
	err := s.fileDBLoad(catalog, database, file, loader)
	if err != nil {
		panic(notebookHalt{err})
	}
}

func (s *session) doltLoadFrom(catalog, name, path string) error {
	path = s.resolveRel(path)

	db, err := dolt.LoadFrom(context.Background(), name, path)
	if err != nil {
		return err
	}

	cat, err := dolt.GetCatalog(catalog, true)
	if err != nil {
		return err
	}

	cat.Attach(db)
	return nil
}

func (s *session) doltMustLoadFrom(catalog, name, path string) {
	err := s.doltLoadFrom(catalog, name, path)
	if err != nil {
		panic(notebookHalt{err})
	}
}
