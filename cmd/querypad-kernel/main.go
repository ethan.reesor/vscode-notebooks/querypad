package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/proto"
	grpc "google.golang.org/grpc"
)

func printUsage() {
	fmt.Fprintf(os.Stderr, `Usage: %[1]s <addr>`, os.Args[0])
	os.Exit(1)
}

func main() {
	if len(os.Args) != 2 {
		printUsage()
	}

	l, err := net.Listen("tcp", os.Args[1])
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	fmt.Printf("Listening on %v\n", l.Addr())

	k := new(kernel)
	s := grpc.NewServer()
	proto.RegisterKernelServer(s, k)

	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
