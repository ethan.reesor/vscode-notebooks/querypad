# QueryPad for VSCode

> Go evaluation powered by [Yaegi](https://github.com/traefik/yaegi)

A VSCode Notebook extension for querying databases.

![sqlite](images/sqlite-memory.png)

## Drivers

By default, the following SQL drivers are compiled in to the kernel:

- [Microsoft SQL Server `sqlserver`|`mssql`](https://pkg.go.dev/github.com/denisenkom/go-mssqldb)
- [MySQL `mysql`](https://pkg.go.dev/github.com/go-sql-driver/mysql)
- [PostgreSQL `postgres`](https://pkg.go.dev/github.com/lib/pq)

### CGo

The following SQL drivers can be included by compling the kernel with `-tags
cgo`. This is disabled by default as it requires CGo.

- [SQLite 3 `sqlite3`](github.com/mattn/go-sqlite3)