module gitlab.com/ethan.reesor/vscode-notebooks/querypad

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/dolthub/dolt/go v0.0.0-20210407201636-01edc18fc218
	github.com/dolthub/go-mysql-server v0.9.1-0.20210408173909-fac6d728eba4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/lib/pq v1.10.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/stretchr/testify v1.6.1
	github.com/traefik/yaegi v0.9.17-0.20210402070804-428b65816019
	google.golang.org/grpc v1.36.1
	google.golang.org/protobuf v1.26.0
)

replace github.com/dolthub/go-mysql-server => github.com/firelizzard18/go-mysql-server v0.9.1-0.20210409032545-7db2fc590783
