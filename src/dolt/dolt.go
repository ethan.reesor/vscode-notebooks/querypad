package dolt

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/dolthub/dolt/go/libraries/doltcore/dbfactory"
	"github.com/dolthub/dolt/go/libraries/doltcore/doltdb"
	"github.com/dolthub/dolt/go/libraries/doltcore/env"
	"github.com/dolthub/dolt/go/libraries/doltcore/sqle"
	"github.com/dolthub/dolt/go/libraries/utils/earl"
	"github.com/dolthub/dolt/go/libraries/utils/filesys"
)

const doltVersion = "0.24.3"

type Database struct {
	sqle.Database
	env *env.DoltEnv
}

func Load(ctx context.Context, name string, env *env.DoltEnv) (*Database, error) {
	database := sqle.NewDatabase(name, env.DbData())
	if env.RSLoadErr != nil {
		return nil, fmt.Errorf("error loading environment: %s", env.RSLoadErr.Error())
	} else if env.DBLoadError != nil {
		return nil, fmt.Errorf("error loading environment: %s", env.DBLoadError.Error())
	} else if env.CfgLoadErr != nil {
		return nil, fmt.Errorf("error loading environment: %s", env.CfgLoadErr.Error())
	}

	return &Database{database, env}, nil
}

func LoadAll(ctx context.Context, menv env.MultiRepoEnv) ([]*Database, error) {
	all := make([]*Database, 0, len(menv))

	err := menv.Iter(func(name string, env *env.DoltEnv) (bool, error) {
		db, err := Load(ctx, name, env)
		if err == nil {
			all = append(all, db)
		}
		return false, err
	})
	if err != nil {
		return nil, err
	}

	return all, nil
}

func LoadFromCurrentDirectory(ctx context.Context, name string) (*Database, error) {
	env := env.Load(ctx, env.GetCurrentUserHomeDir, filesys.LocalFS, doltdb.LocalDirDoltDB, doltVersion)
	return Load(ctx, name, env)
}

func LoadFrom(ctx context.Context, name, path string) (*Database, error) {
	absPath, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	fs, err := filesys.LocalFilesysWithWorkingDir(absPath)
	if err != nil {
		return nil, err
	}

	url := earl.FileUrlFromPath(filepath.Join(absPath, dbfactory.DoltDataDir), os.PathSeparator)
	env := env.Load(ctx, env.GetCurrentUserHomeDir, fs, url, doltVersion)
	return Load(ctx, name, env)
}
