package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"
	"text/tabwriter"

	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/dolt"
)

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	ddb, err := dolt.LoadFromCurrentDirectory(context.Background(), "repo")
	must(err)

	cat, err := dolt.GetCatalog("test", true)
	must(err)
	cat.Attach(ddb)

	db, err := sql.Open("dolt", "test")
	must(err)

	_, err = db.Exec(`USE repo`)
	must(err)

	query := `SHOW TABLES`
	if len(os.Args) > 1 {
		query = os.Args[1]
	}

	rows, err := db.Query(query)
	must(err)

	cols, err := rows.Columns()
	must(err)

	print(cols, rows)
}

func print(sch []string, rows *sql.Rows) {
	vals := make([]interface{}, len(sch))
	ptrs := make([]interface{}, len(sch))
	for i := range sch {
		ptrs[i] = &vals[i]
	}

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	defer w.Flush()

	for _, col := range sch {
		fmt.Fprintf(w, "%s\t", strings.ToUpper(col))
	}
	fmt.Fprintln(w)

	for range sch {
		fmt.Fprintf(w, "---\t")
	}
	fmt.Fprintln(w)

	for rows.Next() {
		must(rows.Scan(ptrs...))

		for _, v := range vals {
			fmt.Fprintf(w, "%v\t", v)
		}
		fmt.Fprintln(w)
	}
}
