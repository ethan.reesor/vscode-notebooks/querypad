package dolt

import (
	"github.com/dolthub/dolt/go/libraries/doltcore/sqle/dfunctions"
	"github.com/dolthub/go-mysql-server/sql"
	"github.com/dolthub/go-mysql-server/sql/information_schema"
)

type Catalog struct {
	name string
	sql  *sql.Catalog
}

func NewCatalog(name string) (*Catalog, error) {
	cat := sql.NewCatalog()
	cat.AddDatabase(information_schema.NewInformationSchemaDatabase(cat))

	err := cat.Register(dfunctions.DoltFunctions...)
	if err != nil {
		return nil, err
	}

	return &Catalog{
		name: name,
		sql:  cat,
	}, nil
}

func (c *Catalog) Name() string { return c.name }

func (c *Catalog) Attach(databases ...*Database) {
	for _, db := range databases {
		c.sql.RemoveDatabase(db.Name())
		c.sql.AddDatabase(db)
	}
}

func (c *Catalog) Detach(databases ...*Database) {
	for _, db := range databases {
		c.sql.RemoveDatabase(db.Name())
	}
}
