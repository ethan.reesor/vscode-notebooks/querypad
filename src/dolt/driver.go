package dolt

import (
	"context"
	stdsql "database/sql"
	"fmt"
	"sync"

	"github.com/dolthub/dolt/go/libraries/doltcore/sqle"
	"github.com/dolthub/go-mysql-server/driver"
	"github.com/dolthub/go-mysql-server/sql"
)

func init() {
	drv := driver.New(factoryImpl{}, driver.Options{})
	stdsql.Register("dolt", drv)
}

var factory struct {
	mu       sync.Mutex
	catalogs map[string]*Catalog
}

func GetCatalog(name string, create bool) (*Catalog, error) {
	factory.mu.Lock()
	defer factory.mu.Unlock()

	catalog, ok := factory.catalogs[name]
	if ok || !create {
		return catalog, nil
	}

	catalog, err := NewCatalog(name)
	if err != nil {
		return nil, err
	}
	attach(catalog)
	return catalog, nil
}

func attach(catalog *Catalog) {
	if factory.catalogs == nil {
		factory.catalogs = map[string]*Catalog{}
	}
	factory.catalogs[catalog.Name()] = catalog
}

func Attach(catalog *Catalog) {
	factory.mu.Lock()
	defer factory.mu.Unlock()
	attach(catalog)
}

func Detach(catalog *Catalog) {
	factory.mu.Lock()
	defer factory.mu.Unlock()
	delete(factory.catalogs, catalog.Name())
}

type factoryImpl struct{}

var _ driver.Provider = factoryImpl{}
var _ driver.SessionBuilder = factoryImpl{}
var _ driver.ContextBuilder = factoryImpl{}

func (factoryImpl) Resolve(name string, options *driver.Options) (string, *sql.Catalog, error) {
	factory.mu.Lock()
	catalog, ok := factory.catalogs[name]
	factory.mu.Unlock()

	if !ok {
		return "", nil, fmt.Errorf("no catalog named %q", name)
	}

	return name, catalog.sql, nil
}

func (factoryImpl) NewSession(ctx context.Context, id uint32, conn *driver.Connector) (sql.Session, error) {
	sqlSess, err := driver.DefaultSessionBuilder{}.NewSession(ctx, id, conn)
	if err != nil {
		return nil, err
	}

	sess, err := sqle.NewDoltSession(ctx, sqlSess, "hello", "world")
	if err != nil {
		return nil, err
	}

	for _, db := range conn.Catalog().AllDatabases() {
		if dolt, ok := db.(*Database); ok {
			err = sess.AddDB(ctx, dolt.Database)
			if err != nil {
				return nil, err
			}
		}
	}

	return sess, nil
}

func (factoryImpl) NewContext(ctx context.Context, conn *driver.Conn, opts ...sql.ContextOption) (*sql.Context, error) {
	sctx, err := driver.DefaultContextBuilder{}.NewContext(ctx, conn, opts...)
	if err != nil {
		return nil, err
	}

	for _, db := range conn.Catalog().AllDatabases() {
		if dolt, ok := db.(*Database); ok {
			root, err := dolt.env.WorkingRoot(ctx)
			if err != nil {
				return nil, err
			}

			err = dolt.SetRoot(sctx, root)
			if err != nil {
				return nil, err
			}

			err = sqle.RegisterSchemaFragments(sctx, dolt.Database, root)
			if err != nil {
				return nil, err
			}
		}
	}

	return sctx, nil
}
