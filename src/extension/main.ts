import vscode = require('vscode')
import {
	ConnectionStatusBarProvider,
	execute,
	Process,
	Session,
	setConnection
} from './kernel';

process.on('unhandledRejection', err => {
	console.error(err);
})

const selector: vscode.NotebookSelector = [
	// { viewType: 'markdown-notebook' },
	// { scheme: 'file' },
	{ pattern: "*.md" },
]

export function activate(context: vscode.ExtensionContext) {
	vscode.notebook.registerNotebookCellStatusBarItemProvider(selector, ConnectionStatusBarProvider)
	const controller = vscode.notebook.createNotebookController('querypad-kernel', selector, 'QueryPad Kernel', execute)
	controller.supportedLanguages = ['go', 'sql']
	controller.onDidChangeNotebookAssociation(e => { if (e.selected) return Session.getOrOpen(e.notebook).catch(() => {}) })
	
	context.subscriptions.push(vscode.commands.registerCommand('querypad.kernel.restart', () => {
		Process.kill('SIGTERM')
	}))

	context.subscriptions.push(vscode.commands.registerCommand('querypad.kernel.rebuild', async () => {
        await Process.install()
	}))

	context.subscriptions.push(vscode.commands.registerCommand('querypad.kernel.stopSession', () => {
		const uri = vscode.window.activeNotebookEditor?.document?.uri
		if (uri) Session.stop(uri)
	}))

	context.subscriptions.push(vscode.commands.registerCommand('querypad.cell.selectConnection',
		(cell?: vscode.NotebookCell) => setConnection(cell)
	))
}

export function deactivate() {
    Process.kill('SIGTERM')
}
