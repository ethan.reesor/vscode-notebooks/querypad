import vscode = require('vscode')
import grpc = require('@grpc/grpc-js')
import { KernelClient } from '../proto/kernel_grpc_pb'
import { FromKernel, ToKernel } from '../proto/kernel_pb'
import { getConfig } from './config'
import { GoGRPCProc, GoTool } from './tools'

type Session = grpc.ClientDuplexStream<ToKernel, FromKernel>

const kernelOutput = vscode.window.createOutputChannel('QueryPad Kernel')

export async function execute(cells: vscode.NotebookCell[], controller: vscode.NotebookController) {
    for (const cell of cells) {
        const kernel = await Session.getOrOpen(cell.notebook)
        const task = controller.createNotebookCellExecutionTask(cell)
        task.token.onCancellationRequested(() => Session.interrupt(cell.notebook.uri))
        kernel.execute(task)
    }
}

export async function setConnection(cell?: vscode.NotebookCell) {
    if (cell) {
        const kernel = await Session.getOrOpen(cell.notebook)
        await kernel.setConnection(cell)
        return
    }

    const editor = vscode.window.activeNotebookEditor
    if (!editor) return

    cell = editor.selections
        .flatMap(range => editor.document.getCells(range))
        .find(cell => cell.document.languageId === 'sql')
    if (!cell) return
    
    const kernel = await Session.getOrOpen(editor.document)
    await kernel.setConnection(cell)
}

export const Process = new (class ProcessController {
    private readonly executable = 'querypad-kernel'
    private readonly installAsk = 'The QueryPad notebook kernel is not available'

    private readonly connectTo: number | undefined //= 12345
    
    private proc: GoGRPCProc<KernelClient, Session> | undefined

    public async install(ask = true) {
        this.goTool().install(ask ? this.installAsk : void 0)
    }

    public kill(signal?: NodeJS.Signals | number) {
        return this.proc?.kill(signal)
    }

    public async openSession(uri: vscode.Uri): Promise<Session> {
        if (!await this.ensureLaunched())
            throw new Error('Failed to launch kernel process')
        return this.proc!.open(uri)
    }

    private get module(): string {
        return getConfig().get('kernel.module') || 'gitlab.com/ethan.reesor/vscode-notebooks/querypad/cmd/querypad-kernel'
    }

    private get version(): string {
        return getConfig().get('kernel.version') || 'latest'
    }

    private goTool() {
        return new GoTool(this.executable, this.module, this.version, kernelOutput)
    }

    private async launch() {
        if (this.connectTo) {
            const client = new KernelClient(`localhost:${this.connectTo}`, grpc.credentials.createInsecure())
            this.proc = new GoGRPCProc(null, client)
            return
        }

        const proc = await this.goTool().launch(['localhost:'], this.installAsk)
        if (!proc) return

        proc.on('exit', () => {
            this.proc = void 0
        })

        const port = await GoGRPCProc.start(proc, kernelOutput)
        const client = new KernelClient(`localhost:${port}`, grpc.credentials.createInsecure())
        this.proc = new GoGRPCProc(proc, client)
    }

    private async ensureLaunched() {
        if (this.proc)
            return true

        try {
            await this.launch()
            return true
        } catch (error) {
            kernelOutput.append(error.message)
            vscode.window.showWarningMessage(error.message)
            return false
        }
    }
})

export const Session = new (class SessionController {
    private readonly kernels = new Map<vscode.Uri, SubKernel>()
    private readonly cache = new Map<vscode.Uri, Record<string, string>>()
    private readonly diagnostics = vscode.languages.createDiagnosticCollection()

    public get(uri: vscode.Uri) {
        return this.kernels.get(uri)
    }

    public async getOrOpen(document: vscode.NotebookDocument): Promise<SubKernel> {
        let sub = this.get(document.uri)
        if (sub) return sub

        const cache = this.cache.get(document.uri) || {}
        this.cache.set(document.uri, cache)

        const session = await Process.openSession(document.uri)
        sub = new SubKernel(document, session, this.diagnostics, cache)
        sub.onStopped(() => this.kernels.delete(document.uri))
        this.kernels.set(document.uri, sub)

        return sub
    }

    public stop(uri: vscode.Uri) {
        this.get(uri)?.stop()
    }

    public interrupt(uri: vscode.Uri) {
        this.get(uri)?.interrupt()
    }
})

type ConnectionInfo = FromKernel.Connections.Connection.AsObject

function connectionInfoStr(c: ConnectionInfo): string {
    return `${c.id} (${c.provider || 'unknown'})`
}

class SubKernel {
    private readonly stopped = new vscode.EventEmitter<void>()
    private readonly cellConnections = new Map<vscode.NotebookCell, string | null>()
    private readonly subscriptions: vscode.Disposable[] = [this.stopped]
    public readonly onStopped = this.stopped.event

    private task: ExecutionTask | undefined
    private connections: ConnectionInfo[] = []

    constructor(
        private readonly document: vscode.NotebookDocument,
        private readonly session: Session,
        private readonly diagnostics: vscode.DiagnosticCollection,
        private readonly cache: Record<string, string>,
    ) {
        session.on('data', msg => this.handle(msg))
        session.on('end', () => {
            this.stopped.fire()
            this.subscriptions.forEach(x => x.dispose())
        })

        session.write(
            new ToKernel().setSetup(
                new ToKernel.Setup()
                    .setPath(this.document.uri.fsPath)))

        document.getCells().forEach(c => ConnectionStatusBarProvider.fireChanged(c))

        vscode.workspace.onDidCloseTextDocument(e => {
            if (e.uri === this.document.uri)
                this.stop()
        }, null, this.subscriptions)

        vscode.workspace.onDidOpenTextDocument(e => {
            if (e.notebook !== this.document) return

            const cell = this.document.getCells().find(c => c.document.uri === e.uri)
            if (!cell) return

            ConnectionStatusBarProvider.fireChanged(cell)
        }, null, this.subscriptions)
    }

    public async setConnection(cell: vscode.NotebookCell) {
        if (cell.notebook !== this.document) return

        if (!this.connections.length) {
            await vscode.window.showQuickPick(['No connections available'])
            return
        }
        
        const connMap = Object.fromEntries(this.connections.map(c => [connectionInfoStr(c), c.id]))
        const conn = await vscode.window.showQuickPick(Object.keys(connMap))
        if (!conn) return

        this.cellConnections.set(cell, connMap[conn])
        ConnectionStatusBarProvider.fireChanged(cell)
    }

    public activeConnection(cell: vscode.NotebookCell) {
        const id = this.cellConnections.get(cell)
        return this.connections.find(c => c.id === id)
    }

    interrupt(): void {
        this.send(new ToKernel().setCancelexecute(new ToKernel.CancelExecute))
    }

    stop() {
        this.session.cancel()
    }

    send(msg: ToKernel) {
        this.session.write(msg)
    }

    setDiagnostics(uri: vscode.Uri, diag: vscode.Diagnostic | vscode.Diagnostic[]) {
        if (diag instanceof vscode.Diagnostic)
            diag = [diag]
        this.diagnostics.set(uri, diag)
    }

    async execute(task: vscode.NotebookCellExecutionTask) {
        switch (task.cell.document.languageId) {
        case 'go':
            this.task = new CodeExecutionTask(task, this)
            break
            
        case 'sql':
            const conn = this.cellConnections.get(task.cell)
            if (!conn) {
                const msg = this.connections.length ? 'No connection selected' : 'No connections available'
                task.start()
                task.appendOutput(new vscode.NotebookCellOutput([
                    new vscode.NotebookCellOutputItem('application/x.notebook.stderr', msg)
                ]))
                task.end({ success: false })
                return
            }

            this.task = new QueryExecutionTask(task, this, conn)
            break

        default:
            task.start()
            task.end({ success: false })
            return
        }
        
        task.start({ startTime: this.task.created })
        task.clearOutput()
        this.diagnostics.set(task.cell.document.uri, [])
        try {
            task.end(await this.task.execute())
        } catch (error) {
            vscode.window.showErrorMessage(error.message)
            task.end({ success: false })
        }
        this.task = void 0
    }

    private async handle(msg: FromKernel) {
        switch (msg.getKindCase()) {
        case FromKernel.KindCase.PROMPT:
            {
                const { prompt, placeholder, secret } = msg.getPrompt()!.toObject()
                const answer = await vscode.window.showInputBox({ prompt, placeHolder: placeholder, password: secret })

                const resp = new ToKernel.Prompted()
                if (answer) resp.setValue(answer)

                this.send(new ToKernel().setPrompted(resp))
                break
            }

        case FromKernel.KindCase.READCACHE:
            {
                const { key } = msg.getReadcache()!.toObject()
                const resp = new ToKernel.Cached().setKey(key)

                const value = this.cache[key]
                if (value) resp.setValue(value)
                this.send(new ToKernel().setCached(resp))
                break
            }

        case FromKernel.KindCase.WRITECACHE:
            {
                const { key, value } = msg.getWritecache()!.toObject()
                const resp = new ToKernel.Cached().setKey(key)

                const oldValue = this.cache[key]
                if (oldValue) resp.setValue(oldValue)
                this.send(new ToKernel().setCached(resp))

                if (value)
                    this.cache[key] = value
                else
                    delete this.cache[key]
                break
            }

        case FromKernel.KindCase.CONNECTIONS:
            {
                this.connections = msg.getConnections()!.getConnectionList().map(c => c.toObject())
                if (this.connections.length != 1) break

                this.document.getCells().forEach(cell => {
                    this.cellConnections.set(cell, this.connections[0].id)
                    ConnectionStatusBarProvider.fireChanged(cell)
                })
                break
            }

        default:
            this.task?.handleMessage(msg)
            break
        }
    }
}

abstract class ExecutionTask {
    // @ts-ignore
    protected resolve: (_: vscode.NotebookCellExecuteEndContext) => void
    protected readonly done = new Promise<vscode.NotebookCellExecuteEndContext>((r, j) => (this.resolve = r))
    public readonly created = Date.now()

    constructor(
        protected readonly task: vscode.NotebookCellExecutionTask,
        protected readonly kernel: SubKernel,
    ) {}

    get uri() { return this.task.cell.document.uri }

    abstract execute(): Promise<vscode.NotebookCellExecuteEndContext>

    async handleMessage(msg: FromKernel) {
        switch (msg.getKindCase()) {
        case FromKernel.KindCase.EXECUTED:
            {
                const resp = msg.getExecuted()!.toObject()
                const result: vscode.NotebookCellExecuteEndContext = {
                    success: true,
                }

                if (resp.duration) {
                    const { seconds, nanos } = resp.duration
                    result.endTime = this.created + seconds * 1000 + nanos / 1000000
                } else {
                    result.endTime = Date.now()
                }

                for (const err of resp.errorsList) {
                    result.success = false

                    if (err.position) {
                        const lineNo = err.position.line - 1
                        const column = err.position.column - 1
                        const line = this.task.cell.document.lineAt(lineNo)                   

                        this.kernel.setDiagnostics(this.uri, [
                            new vscode.Diagnostic(new vscode.Range(lineNo, column, lineNo, line.text.length - column), err.message, vscode.DiagnosticSeverity.Error)
                        ])
                    }

                    this.appendOutput({ 'application/x.notebook.stderr': err.message })
                }

                this.resolve(result)
            }
            break

        case FromKernel.KindCase.OUTPUT:
            {
                const items = msg.getOutput()!.getContentList().map(c => {
                    let mime = c.getType()
                    let value = c.getValue()
                    
                    value = Buffer.from(value).toString('utf-8')
    
                    switch (mime) {
                    case 'stdout':
                        mime = 'application/x.notebook.stdout'
                        break
    
                    case 'stderr':
                        mime = 'application/x.notebook.stderr'
                        break
    
                    case 'application/json':
                        try {
                            value = JSON.parse(value)
                        } catch (e) {}
                        break
                    }

                    return [mime, value]
                })
                
                this.appendOutput(Object.fromEntries(items))
            }
            break
        }
    }

    protected appendOutput(content: Record<string, string>) {
        const items = Object.entries(content).map(([mime, value]) => new vscode.NotebookCellOutputItem(mime, value))
        this.task.appendOutput(new vscode.NotebookCellOutput(items))
    }
}

class CodeExecutionTask extends ExecutionTask {
    async execute() {
        this.kernel.onStopped(() => this.resolve({}))

        this.kernel.send(
            new ToKernel().setExecutecode(
                new ToKernel.ExecuteCode().setCode(
                    this.task.cell.document.getText())))

        return await this.done
    }
}

class QueryExecutionTask extends ExecutionTask {
    constructor(
        task: vscode.NotebookCellExecutionTask,
        kernel: SubKernel,
        private readonly connection: string,
    ) {
        super(task, kernel)
    }

    async execute() {
        this.kernel.onStopped(() => this.resolve({}))

        this.kernel.send(
            new ToKernel().setExecutequery(
                new ToKernel.ExecuteQuery()
                    .setConnection(this.connection)
                    .setQuery(this.task.cell.document.getText())))

        return await this.done
    }
}

export const ConnectionStatusBarProvider = new (class ConnectionStatusBarProvider implements vscode.NotebookCellStatusBarItemProvider {
    private readonly changed = new vscode.EventEmitter<void>()
    public readonly onDidChangeCellStatusBarItems = this.changed.event

    fireChanged(cell: vscode.NotebookCell) {
        this.changed.fire()
    }

    provideCellStatusBarItems(cell: vscode.NotebookCell, token: vscode.CancellationToken): vscode.NotebookCellStatusBarItem[] {
        if (cell.document.languageId !== 'sql')
            return []

        const conn = Session.get(cell.notebook.uri)?.activeConnection(cell)
        const text = conn ? connectionInfoStr(conn) : '<disconnected>'
        return [new vscode.NotebookCellStatusBarItem(text, vscode.NotebookCellStatusBarAlignment.Right, 'querypad.cell.selectConnection', 'QueryPad connection')]
    }
})