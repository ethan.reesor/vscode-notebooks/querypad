package interpreter

import (
	"context"
	"errors"
	"reflect"
	"sync"
	"time"
)

var ErrCanceled = errors.New("canceled")

type EvalOptions struct {
	OnStdout func([]byte)
	OnStderr func([]byte)
	OnPanic  func(interface{})
}

func (s *Session) Eval(ctx context.Context, code string, opts EvalOptions) (reflect.Value, time.Duration, error) {
	if opts.OnPanic != nil {
		defer func() {
			if r := recover(); r != nil {
				opts.OnPanic(r)
			}
		}()
	}

	var wg sync.WaitGroup
	defer wg.Wait()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	if opts.OnStdout != nil {
		wg.Add(1)
		go s.forward(ctx, &wg, s.stdout, opts.OnStdout)
	}

	if opts.OnStderr != nil {
		wg.Add(1)
		go s.forward(ctx, &wg, s.stderr, opts.OnStderr)
	}

	before := time.Now()
	v, err := s.interpreter.EvalWithContext(ctx, code)
	after := time.Now()
	duration := after.Sub(before)

	select {
	case <-ctx.Done():
		return v, duration, ErrCanceled
	default:
		return v, duration, err
	}
}

func (s *Session) forward(ctx context.Context, wg *sync.WaitGroup, ch <-chan []byte, cb func([]byte)) {
	defer wg.Done()

	for {
		select {
		case <-ctx.Done():
			return

		case b := <-ch:
			cb(b)
		}
	}
}
