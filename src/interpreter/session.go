package interpreter

import (
	"bytes"
	"context"
	"go/build"
	"io"
	"log"
	"os"

	"github.com/traefik/yaegi/interp"
)

type Session struct {
	interpreter *interp.Interpreter
	context     context.Context
	cancel      context.CancelFunc
	stdout      chan []byte
	stderr      chan []byte
}

func NewSession(ctx context.Context) *Session {
	s := new(Session)
	s.context, s.cancel = context.WithCancel(ctx)

	rOut, wOut := io.Pipe()
	rErr, wErr := io.Pipe()

	s.stdout = make(chan []byte)
	s.stderr = make(chan []byte)

	go func() {
		<-s.context.Done()
		rOut.Close()
		wOut.Close()
		rErr.Close()
		wErr.Close()
	}()

	go s.read(rOut, s.stdout)
	go s.read(rErr, s.stderr)

	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		gopath = build.Default.GOPATH
	}

	s.interpreter = interp.New(interp.Options{
		Stdin:  bytes.NewReader(nil),
		Stdout: wOut,
		Stderr: wErr,
		GoPath: gopath,
	})

	return s
}

func (s *Session) read(r io.Reader, ch chan<- []byte) {
	defer close(ch)

	var b [512]byte
	for {
		n, err := r.Read(b[:])
		if err != nil {
			log.Println(err)
			return
		}

		d := make([]byte, n)
		copy(d, b[:n])
		ch <- d
	}
}

func (s *Session) Use(exports interp.Exports) {
	s.interpreter.Use(exports)
}
