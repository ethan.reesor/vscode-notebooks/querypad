package interpreter

import (
	"reflect"

	"github.com/traefik/yaegi/interp"
)

type exportBuilder struct {
	s      *Session
	name   string
	values map[string]reflect.Value
}

func (s *Session) Package(name string) *exportBuilder {
	return &exportBuilder{s, name, map[string]reflect.Value{}}
}

func (ex *exportBuilder) Add(name string, value interface{}) *exportBuilder {
	ex.values[name] = reflect.ValueOf(value)
	return ex
}

func (ex *exportBuilder) Export() {
	ex.s.interpreter.Use(interp.Exports{ex.name: ex.values})
}
