package filedb

import (
	"database/sql"
	"errors"

	"github.com/dolthub/go-mysql-server/driver"
)

var ErrSchemaMismatch = errors.New("schema does not match")
var ErrMalformedFile = errors.New("malformed file")

func init() {
	drv := driver.New(factoryImpl{}, driver.Options{})
	sql.Register("filedb", drv)
}
