package filedb

import (
	"io"

	"github.com/dolthub/go-mysql-server/sql"
)

type noopPartitionIter struct{ done bool }

func (*noopPartitionIter) Close(*sql.Context) error { return nil }

func (p *noopPartitionIter) Next() (sql.Partition, error) {
	if p.done {
		return nil, io.EOF
	}
	p.done = true

	return noopPartition{}, nil
}

type noopPartition struct{}

func (noopPartition) Key() []byte { return []byte("1") }
