package csv

import "encoding/csv"

type Writer = csv.Writer
type ParseError = csv.ParseError

var ErrTrailingComma = csv.ErrTrailingComma
var ErrBareQuote = csv.ErrBareQuote
var ErrQuote = csv.ErrQuote
var ErrFieldCount = csv.ErrFieldCount

var NewWriter = csv.NewWriter
