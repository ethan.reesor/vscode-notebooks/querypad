package csv

import (
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReaderPos(t *testing.T) {
	type Result struct {
		Before, After int64
		Record        []string
	}

	cases := []struct {
		Name, Contents string
		Expect         []Result
	}{
		{"Long", "foo\nbar1\nbaz123\nbat12\n", []Result{
			{0, 4, []string{"foo"}},
			{4, 9, []string{"bar1"}},
			{9, 16, []string{"baz123"}},
			{16, 22, []string{"bat12"}},
		}},
		{"Long-ExtraNL", "foo\nbar1\nbaz123\nbat12\n\n", []Result{
			{0, 4, []string{"foo"}},
			{4, 9, []string{"bar1"}},
			{9, 16, []string{"baz123"}},
			{16, 22, []string{"bat12"}},
		}},
		{"Long-BlankLine", "foo\nbar1\n\nbaz123\nbat12\n", []Result{
			{0, 4, []string{"foo"}},
			{4, 9, []string{"bar1"}},
			{10, 17, []string{"baz123"}},
			{17, 23, []string{"bat12"}},
		}},
		{"Long-NoNL", "foo\nbar1\nbaz123\nbat12", []Result{
			{0, 4, []string{"foo"}},
			{4, 9, []string{"bar1"}},
			{9, 16, []string{"baz123"}},
			{16, 21, []string{"bat12"}},
		}},
		{"Empty", "", []Result{}},
		{"Short", "foo\n", []Result{
			{0, 4, []string{"foo"}},
		}},
		{"Short-NoNL", "foo\n", []Result{
			{0, 4, []string{"foo"}},
		}},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			r := NewReader(strings.NewReader(c.Contents))

			for i := 0; true; i++ {
				rec, before, after, err := r.Read()
				if err == io.EOF {
					break
				}
				require.NoError(t, err)

				x := c.Expect[i]
				require.Equal(t, x.Record, rec, "Record")
				require.Equal(t, x.Before, before, "Before")
				require.Equal(t, x.After, after, "After")
			}
		})
	}
}
