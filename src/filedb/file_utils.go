package filedb

import (
	"io"
)

func hasTrailingNewLine(file io.ReadSeeker) (bool, error) {
	_, err := file.Seek(-1, io.SeekEnd)
	if err != nil {
		return false, err
	}

	var buf [1]byte
	_, err = file.Read(buf[:])
	if err != nil {
		return false, err
	}
	return buf[0] == '\n', nil
}
