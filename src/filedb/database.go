package filedb

import (
	"strings"
	"sync"

	"github.com/dolthub/go-mysql-server/sql"
)

type Database struct {
	name   string
	mu     sync.Mutex
	tables map[string]Table
}

func NewDatabase(name string) *Database {
	db := &Database{name: name}
	return db
}

func (db *Database) Name() string {
	return db.name
}

func (db *Database) GetTableNames(*sql.Context) ([]string, error) {
	db.mu.Lock()
	defer db.mu.Unlock()

	names := make([]string, 0, len(db.tables))
	for name := range db.tables {
		names = append(names, name)
	}
	return names, nil
}

func (db *Database) GetTableInsensitive(ctx *sql.Context, name string) (sql.Table, bool, error) {
	if table, ok := db.tables[name]; ok {
		return table, true, nil
	}

	name = strings.ToLower(name)
	for k, table := range db.tables {
		if name == strings.ToLower(k) {
			return table, true, nil
		}
	}

	return nil, false, nil
}

func (db *Database) Attach(table Table) error {
	db.mu.Lock()
	defer db.mu.Unlock()

	if db.tables == nil {
		db.tables = map[string]Table{}
	}
	db.tables[table.Name()] = table
	return nil
}

func (db *Database) Detach(table Table) {
	db.mu.Lock()
	defer db.mu.Unlock()
	delete(db.tables, table.Name())
}

func (db *Database) Load(file string, loader TableLoader) (Table, error) {
	table, err := loader.Load(file)
	if err != nil {
		return nil, err
	}

	err = db.Attach(table)
	if err != nil {
		return nil, err
	}

	return table, nil
}
