package filedb

import (
	"fmt"
	"sync"

	"github.com/dolthub/go-mysql-server/driver"
	"github.com/dolthub/go-mysql-server/sql"
)

var factory struct {
	mu       sync.Mutex
	catalogs map[string]*Catalog
}

type factoryImpl struct{}

func (factoryImpl) Resolve(name string, _ *driver.Options) (string, *sql.Catalog, error) {
	factory.mu.Lock()
	catalog, ok := factory.catalogs[name]
	factory.mu.Unlock()

	if !ok {
		return "", nil, fmt.Errorf("no catalog named %q", name)
	}

	return name, catalog.sql, nil
}

func GetCatalog(name string, create bool) *Catalog {
	factory.mu.Lock()
	defer factory.mu.Unlock()

	catalog, ok := factory.catalogs[name]
	if ok || !create {
		return catalog
	}

	catalog = NewCatalog(name)
	attach(catalog)
	return catalog
}

func attach(catalog *Catalog) {
	if factory.catalogs == nil {
		factory.catalogs = map[string]*Catalog{}
	}
	factory.catalogs[catalog.Name()] = catalog
}

func Attach(catalog *Catalog) {
	factory.mu.Lock()
	defer factory.mu.Unlock()
	attach(catalog)
}

func Detach(catalog *Catalog) {
	factory.mu.Lock()
	defer factory.mu.Unlock()
	delete(factory.catalogs, catalog.Name())
}
