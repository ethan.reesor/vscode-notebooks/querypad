package filedb

import (
	"github.com/dolthub/go-mysql-server/sql"
)

type Table interface {
	sql.Table
}

type TableLoader interface {
	Load(file string) (Table, error)
}
