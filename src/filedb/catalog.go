package filedb

import (
	"sync"

	"github.com/dolthub/go-mysql-server/sql"
	"github.com/dolthub/go-mysql-server/sql/information_schema"
)

type Catalog struct {
	name      string
	mu        sync.Mutex
	sql       *sql.Catalog
	databases map[string]*Database
}

func NewCatalog(name string) *Catalog {
	cat := sql.NewCatalog()
	cat.AddDatabase(information_schema.NewInformationSchemaDatabase(cat))
	return &Catalog{
		name: name,
		sql:  cat,
	}
}

func (c *Catalog) Name() string { return c.name }

func (c *Catalog) GetDatabase(name string, create bool) *Database {
	c.mu.Lock()
	defer c.mu.Unlock()

	database, ok := c.databases[name]
	if ok || !create {
		return database
	}

	database = NewDatabase(name)
	c.attach(database)
	return database
}

func (c *Catalog) attach(database *Database) {
	c.sql.RemoveDatabase(database.Name())

	if c.databases == nil {
		c.databases = map[string]*Database{}
	}
	c.databases[database.Name()] = database
	c.sql.AddDatabase(database)
}

func (c *Catalog) Attach(database *Database) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.attach(database)
}

func (c *Catalog) Detach(database *Database) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.sql.RemoveDatabase(database.Name())
	delete(c.databases, database.Name())
}
