package filedb

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/dolthub/go-mysql-server/sql"
	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/filedb/csv"
)

var CSV CsvLoader

type CsvLoader struct {
	name     string
	headings []string
}

func (l CsvLoader) WithName(name string) CsvLoader {
	l.name = name
	return l
}

func (l CsvLoader) WithHeadings(headings ...string) CsvLoader {
	l.headings = headings
	return l
}

func (l CsvLoader) Load(file string) (Table, error) {
	t := &CsvTable{
		name: l.name,
		file: file,
	}

	if t.name == "" {
		base := filepath.Base(file)
		ext := filepath.Ext(base)
		t.name = base[:len(base)-len(ext)]
	}

	err := t.verify(l.headings)
	if err != nil {
		return nil, err
	}

	return t, nil
}

type CsvTable struct {
	name   string
	file   string
	schema sql.Schema
}

var _ sql.Table = (*CsvTable)(nil)
var _ sql.InsertableTable = (*CsvTable)(nil)
var _ sql.UpdatableTable = (*CsvTable)(nil)

func (t *CsvTable) verify(headings []string) error {
	f, err := os.Open(t.file)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return err
		}

		if headings == nil {
			return fmt.Errorf("%q does not exist: headings must be specified", t.file)
		}

		return t.writeHeadings(headings)
	}
	defer f.Close()

	first, _, _, err := csv.NewReader(f).Read()
	if err != nil {
		if !errors.Is(err, io.EOF) {
			return err
		}

		if headings == nil {
			return fmt.Errorf("%q is empty: headings must be specified", t.file)
		}

		return t.writeHeadings(headings)
	}

	if headings == nil {
		t.setSchema(first)
		return nil
	} else {
		t.setSchema(headings)
	}

	if len(first) != len(headings) {
		return ErrSchemaMismatch
	}
	for i := range first {
		if first[i] != headings[i] {
			return ErrSchemaMismatch
		}
	}
	return nil
}

func (t *CsvTable) verifyCSV(r *csv.Reader) error {
	first, _, _, err := r.Read()
	if err != nil {
		return err
	}

	if len(first) != len(t.schema) {
		return ErrSchemaMismatch
	}
	for i := range first {
		if first[i] != t.schema[i].Name {
			return ErrSchemaMismatch
		}
	}
	return nil
}

func (t *CsvTable) setSchema(headings []string) {
	t.schema = make(sql.Schema, len(headings))
	for i := range headings {
		t.schema[i] = &sql.Column{
			Name:   headings[i],
			Type:   sql.Text,
			Source: t.name,
		}
	}
}

func (t *CsvTable) writeHeadings(headings []string) error {
	t.setSchema(headings)

	f, err := os.Create(t.file)
	if err != nil {
		return err
	}
	defer f.Close()

	w := csv.NewWriter(f)
	defer w.Flush()
	return w.Write(headings)
}

func (t *CsvTable) openReader() (*os.File, *csv.Reader, error) {
	f, err := os.OpenFile(t.file, os.O_RDONLY, 0)
	if err != nil {
		return nil, nil, err
	}

	r := csv.NewReader(f)
	r.ReuseRecord = true
	err = t.verifyCSV(r)
	if err != nil {
		return nil, nil, err
	}

	return f, r, nil
}

func (t *CsvTable) Name() string { return t.name }

func (t *CsvTable) String() string { return t.file }

func (t *CsvTable) Schema() sql.Schema { return t.schema }

func (t *CsvTable) Partitions(*sql.Context) (sql.PartitionIter, error) {
	return &noopPartitionIter{}, nil
}

func (t *CsvTable) PartitionRows(*sql.Context, sql.Partition) (sql.RowIter, error) {
	f, r, err := t.openReader()
	if err != nil {
		return nil, err
	}
	return &CsvRowIter{f, r, t.schema}, nil
}

func (t *CsvTable) Inserter(*sql.Context) sql.RowInserter {
	return &CsvRowInserter{path: t.file}
}

func (t *CsvTable) Updater(*sql.Context) sql.RowUpdater {
	return &CsvRowUpdater{t}
}

func (t *CsvTable) Deleter(*sql.Context) sql.RowDeleter {
	return &CsvRowDeleter{t}
}

type CsvRowIter struct {
	file   io.Closer
	csv    *csv.Reader
	schema sql.Schema
}

func (r *CsvRowIter) Close(*sql.Context) error {
	return r.file.Close()
}

func (r *CsvRowIter) Next() (sql.Row, error) {
again:
	csv, _, _, err := r.csv.Read()
	if err != nil {
		return nil, err
	}

	if len(csv) == 0 {
		goto again
	}

	if len(csv) != len(r.schema) {
		return nil, ErrMalformedFile
	}

	row := make(sql.Row, len(csv))
	for i := range csv {
		row[i] = csv[i]
	}
	return row, nil
}

func sqlRowToCSV(row sql.Row) []string {
	str := make([]string, len(row))
	for i := range str {
		str[i] = fmt.Sprint(row[i])
	}
	return str
}

type CsvRowInserter struct {
	path   string
	file   *os.File
	writer *csv.Writer
}

func (r *CsvRowInserter) Close(*sql.Context) error {
	r.writer.Flush()
	return r.file.Close()
}

func (r *CsvRowInserter) Insert(ctx *sql.Context, row sql.Row) error {
	str := sqlRowToCSV(row)
	if r.writer != nil {
		return r.writer.Write(str)
	}

	var err error
	r.file, err = os.OpenFile(r.path, os.O_RDWR, 0)
	if err != nil {
		return err
	}

	ok, err := hasTrailingNewLine(r.file)
	if err == nil && !ok {
		_, err = r.file.Write([]byte{'\n'})
	}
	if err != nil {
		return err
	}

	r.writer = csv.NewWriter(r.file)
	return r.writer.Write(str)
}

func csvFindRow(path string, row sql.Row) (before, after int64, err error) {
	f, err := os.OpenFile(path, os.O_RDONLY, 0)
	if err != nil {
		return 0, 0, err
	}
	defer f.Close()

	csvr := csv.NewReader(f)
outer:
	for {
		rec, before, after, err := csvr.Read()
		if err != nil {
			return 0, 0, err
		}

		if len(rec) != len(row) {
			continue
		}

		for i := range row {
			if fmt.Sprint(row[i]) != rec[i] {
				continue outer
			}
		}

		return before, after, nil
	}
}

func csvPatchRow(path string, row sql.Row, patch func(io.Writer) error) error {
	before, after, err := csvFindRow(path, row)
	if err != nil {
		return err
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	temp, err := os.Create(path + "~")
	if err != nil {
		return err
	}
	defer temp.Close()

	// copy lines before record
	_, err = temp.ReadFrom(&io.LimitedReader{R: file, N: before})
	if err != nil {
		return err
	}

	// do the thing
	if patch != nil {
		err = patch(temp)
		if err != nil {
			return err
		}
	}

	// copy lines after record
	_, err = file.Seek(after, io.SeekStart)
	if err != nil {
		return err
	}
	_, err = temp.ReadFrom(file)
	if err != nil {
		return err
	}

	file.Close()
	err = temp.Close()
	if err != nil {
		return err
	}

	// replace old file with new
	err = os.Rename(temp.Name(), file.Name())
	return err
}

type CsvRowUpdater struct {
	table *CsvTable
}

func (r *CsvRowUpdater) Close(*sql.Context) error {
	return nil
}

// Update the given row. Provides both the old and new rows.
func (r *CsvRowUpdater) Update(ctx *sql.Context, old sql.Row, new sql.Row) error {
	return csvPatchRow(r.table.file, old, func(f io.Writer) error {
		// write the updated record
		w := csv.NewWriter(f)
		err := w.Write(sqlRowToCSV(new))
		w.Flush()
		return err
	})
}

type CsvRowDeleter struct {
	table *CsvTable
}

func (r *CsvRowDeleter) Close(*sql.Context) error {
	return nil
}

func (r *CsvRowDeleter) Delete(ctx *sql.Context, row sql.Row) error {
	return csvPatchRow(r.table.file, row, nil)
}
