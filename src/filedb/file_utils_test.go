package filedb

import (
	"strings"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/require"
)

func TestHasTrailingNewLine(t *testing.T) {
	cases := []struct {
		Name, Contents string
		Expect         bool
	}{
		{"Non-Trailing", "foo", false},
		{"Trailing", "foo\n", true},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			r := strings.NewReader(c.Contents)
			ok, err := hasTrailingNewLine(r)
			if err != nil {
				spew.Dump(err)
			}
			require.NoError(t, err)

			if c.Expect {
				require.True(t, ok)
			} else {
				require.False(t, ok)
			}
		})
	}
}
