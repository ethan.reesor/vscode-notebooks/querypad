package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"gitlab.com/ethan.reesor/vscode-notebooks/querypad/src/filedb"
)

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	fdb := filedb.NewDatabase("testDB")
	cat := filedb.NewCatalog("testCat")
	cat.Attach(fdb)
	must(filedb.Attach(cat))

	_, err := fdb.Load("test.csv", filedb.CSV)
	must(err)

	db, err := sql.Open("filedb", "testCat")
	must(err)

	_, err = db.Exec("INSERT INTO testDB.test VALUES ('asdf', 'qwer')")
	must(err)
	time.Sleep(time.Second)

	_, err = db.Exec("UPDATE testDB.test SET bar='rtyu' WHERE foo='asdf'")
	must(err)
	time.Sleep(time.Second)

	_, err = db.Exec("DELETE FROM testDB.test WHERE foo='asdf'")
	must(err)

	rows, err := db.Query("SELECT * FROM testDB.test")
	must(err)

	var foo, bar string
	for rows.Next() {
		must(rows.Scan(&foo, &bar))
		fmt.Println(foo, bar)
	}
}
