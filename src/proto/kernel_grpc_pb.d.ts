// package: 
// file: kernel.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as kernel_pb from "./kernel_pb";
import * as google_protobuf_duration_pb from "google-protobuf/google/protobuf/duration_pb";

interface IKernelService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    session: IKernelService_ISession;
}

interface IKernelService_ISession extends grpc.MethodDefinition<kernel_pb.ToKernel, kernel_pb.FromKernel> {
    path: "/Kernel/Session";
    requestStream: true;
    responseStream: true;
    requestSerialize: grpc.serialize<kernel_pb.ToKernel>;
    requestDeserialize: grpc.deserialize<kernel_pb.ToKernel>;
    responseSerialize: grpc.serialize<kernel_pb.FromKernel>;
    responseDeserialize: grpc.deserialize<kernel_pb.FromKernel>;
}

export const KernelService: IKernelService;

export interface IKernelServer extends grpc.UntypedServiceImplementation {
    session: grpc.handleBidiStreamingCall<kernel_pb.ToKernel, kernel_pb.FromKernel>;
}

export interface IKernelClient {
    session(): grpc.ClientDuplexStream<kernel_pb.ToKernel, kernel_pb.FromKernel>;
    session(options: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<kernel_pb.ToKernel, kernel_pb.FromKernel>;
    session(metadata: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<kernel_pb.ToKernel, kernel_pb.FromKernel>;
}

export class KernelClient extends grpc.Client implements IKernelClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public session(options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<kernel_pb.ToKernel, kernel_pb.FromKernel>;
    public session(metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<kernel_pb.ToKernel, kernel_pb.FromKernel>;
}
