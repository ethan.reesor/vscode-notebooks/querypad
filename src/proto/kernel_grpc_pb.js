// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var kernel_pb = require('./kernel_pb.js');
var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');

function serialize_FromKernel(arg) {
  if (!(arg instanceof kernel_pb.FromKernel)) {
    throw new Error('Expected argument of type FromKernel');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_FromKernel(buffer_arg) {
  return kernel_pb.FromKernel.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ToKernel(arg) {
  if (!(arg instanceof kernel_pb.ToKernel)) {
    throw new Error('Expected argument of type ToKernel');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ToKernel(buffer_arg) {
  return kernel_pb.ToKernel.deserializeBinary(new Uint8Array(buffer_arg));
}


var KernelService = exports.KernelService = {
  session: {
    path: '/Kernel/Session',
    requestStream: true,
    responseStream: true,
    requestType: kernel_pb.ToKernel,
    responseType: kernel_pb.FromKernel,
    requestSerialize: serialize_ToKernel,
    requestDeserialize: deserialize_ToKernel,
    responseSerialize: serialize_FromKernel,
    responseDeserialize: deserialize_FromKernel,
  },
};

exports.KernelClient = grpc.makeGenericClientConstructor(KernelService);
