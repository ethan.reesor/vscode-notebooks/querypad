package proto

import (
	"time"

	"google.golang.org/protobuf/types/known/durationpb"
)

//go:generate protoc -I../.. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative kernel.proto

func String(v string) *string {
	if v == "" {
		return nil
	}
	return &v
}

func (msg *ToKernel) Get() interface{} {
	switch v := msg.Kind.(type) {
	case *ToKernel_Setup_:
		return v.Setup
	case *ToKernel_ExecuteCode_:
		return v.ExecuteCode
	case *ToKernel_ExecuteQuery_:
		return v.ExecuteQuery
	case *ToKernel_CancelExecute_:
		return v.CancelExecute
	case *ToKernel_Cached_:
		return v.Cached
	case *ToKernel_Prompted_:
		return v.Prompted
	case *ToKernel_ListConnections_:
		return v.ListConnections
	default:
		return v
	}
}

func (msg *FromKernel) Output(content ...*Content) {
	msg.Kind = &FromKernel_Output_{
		Output: &FromKernel_Output{
			Content: content,
		},
	}
}

func (msg *FromKernel) Executed(duration time.Duration, errors []*Error) {
	msg.Kind = &FromKernel_Executed_{
		Executed: &FromKernel_Executed{
			Duration: durationpb.New(duration),
			Errors:   errors,
		},
	}
}

func (msg *FromKernel) ReadCache(key string) {
	msg.Kind = &FromKernel_ReadCache_{
		ReadCache: &FromKernel_ReadCache{
			Key: key,
		},
	}
}

func (msg *FromKernel) WriteCache(key, value string) {
	msg.Kind = &FromKernel_WriteCache_{
		WriteCache: &FromKernel_WriteCache{
			Key:   key,
			Value: String(value),
		},
	}
}
func (msg *FromKernel) Prompt(prompt, placeholder string, secret bool) {
	msg.Kind = &FromKernel_Prompt_{
		Prompt: &FromKernel_Prompt{
			Prompt:      String(prompt),
			Placeholder: String(placeholder),
			Secret:      secret,
		},
	}
}

func (msg *FromKernel) Connections(connections []*FromKernel_Connections_Connection) {
	msg.Kind = &FromKernel_Connections_{
		Connections: &FromKernel_Connections{
			Connection: connections,
		},
	}
}

func (msg *FromKernel_Executed) AddError(message, stack string) {
	msg.Errors = append(msg.Errors, &Error{
		Message: message,
		Stack:   String(stack),
	})
}

func (msg *FromKernel_Executed) AddErrorAt(message, stack string, line, col int) {
	msg.Errors = append(msg.Errors, &Error{
		Message:  message,
		Stack:    String(stack),
		Position: &Error_LineAndColumn{Line: int32(line), Column: int32(col)},
	})
}

func (msg *FromKernel_Executed) SetDuration(d time.Duration) {
	msg.Duration = durationpb.New(d)
}
