// package: 
// file: kernel.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as google_protobuf_duration_pb from "google-protobuf/google/protobuf/duration_pb";

export class ToKernel extends jspb.Message { 

    hasSetup(): boolean;
    clearSetup(): void;
    getSetup(): ToKernel.Setup | undefined;
    setSetup(value?: ToKernel.Setup): ToKernel;

    hasExecutecode(): boolean;
    clearExecutecode(): void;
    getExecutecode(): ToKernel.ExecuteCode | undefined;
    setExecutecode(value?: ToKernel.ExecuteCode): ToKernel;

    hasExecutequery(): boolean;
    clearExecutequery(): void;
    getExecutequery(): ToKernel.ExecuteQuery | undefined;
    setExecutequery(value?: ToKernel.ExecuteQuery): ToKernel;

    hasCancelexecute(): boolean;
    clearCancelexecute(): void;
    getCancelexecute(): ToKernel.CancelExecute | undefined;
    setCancelexecute(value?: ToKernel.CancelExecute): ToKernel;

    hasCached(): boolean;
    clearCached(): void;
    getCached(): ToKernel.Cached | undefined;
    setCached(value?: ToKernel.Cached): ToKernel;

    hasPrompted(): boolean;
    clearPrompted(): void;
    getPrompted(): ToKernel.Prompted | undefined;
    setPrompted(value?: ToKernel.Prompted): ToKernel;

    hasListconnections(): boolean;
    clearListconnections(): void;
    getListconnections(): ToKernel.ListConnections | undefined;
    setListconnections(value?: ToKernel.ListConnections): ToKernel;

    getKindCase(): ToKernel.KindCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ToKernel.AsObject;
    static toObject(includeInstance: boolean, msg: ToKernel): ToKernel.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ToKernel, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ToKernel;
    static deserializeBinaryFromReader(message: ToKernel, reader: jspb.BinaryReader): ToKernel;
}

export namespace ToKernel {
    export type AsObject = {
        setup?: ToKernel.Setup.AsObject,
        executecode?: ToKernel.ExecuteCode.AsObject,
        executequery?: ToKernel.ExecuteQuery.AsObject,
        cancelexecute?: ToKernel.CancelExecute.AsObject,
        cached?: ToKernel.Cached.AsObject,
        prompted?: ToKernel.Prompted.AsObject,
        listconnections?: ToKernel.ListConnections.AsObject,
    }


    export class Setup extends jspb.Message { 
        getPath(): string;
        setPath(value: string): Setup;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Setup.AsObject;
        static toObject(includeInstance: boolean, msg: Setup): Setup.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Setup, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Setup;
        static deserializeBinaryFromReader(message: Setup, reader: jspb.BinaryReader): Setup;
    }

    export namespace Setup {
        export type AsObject = {
            path: string,
        }
    }

    export class ExecuteCode extends jspb.Message { 
        getCode(): string;
        setCode(value: string): ExecuteCode;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): ExecuteCode.AsObject;
        static toObject(includeInstance: boolean, msg: ExecuteCode): ExecuteCode.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: ExecuteCode, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): ExecuteCode;
        static deserializeBinaryFromReader(message: ExecuteCode, reader: jspb.BinaryReader): ExecuteCode;
    }

    export namespace ExecuteCode {
        export type AsObject = {
            code: string,
        }
    }

    export class ExecuteQuery extends jspb.Message { 
        getQuery(): string;
        setQuery(value: string): ExecuteQuery;
        getConnection(): string;
        setConnection(value: string): ExecuteQuery;

        hasDatabase(): boolean;
        clearDatabase(): void;
        getDatabase(): string | undefined;
        setDatabase(value: string): ExecuteQuery;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): ExecuteQuery.AsObject;
        static toObject(includeInstance: boolean, msg: ExecuteQuery): ExecuteQuery.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: ExecuteQuery, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): ExecuteQuery;
        static deserializeBinaryFromReader(message: ExecuteQuery, reader: jspb.BinaryReader): ExecuteQuery;
    }

    export namespace ExecuteQuery {
        export type AsObject = {
            query: string,
            connection: string,
            database?: string,
        }
    }

    export class CancelExecute extends jspb.Message { 

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): CancelExecute.AsObject;
        static toObject(includeInstance: boolean, msg: CancelExecute): CancelExecute.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: CancelExecute, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): CancelExecute;
        static deserializeBinaryFromReader(message: CancelExecute, reader: jspb.BinaryReader): CancelExecute;
    }

    export namespace CancelExecute {
        export type AsObject = {
        }
    }

    export class Cached extends jspb.Message { 
        getKey(): string;
        setKey(value: string): Cached;

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): Cached;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Cached.AsObject;
        static toObject(includeInstance: boolean, msg: Cached): Cached.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Cached, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Cached;
        static deserializeBinaryFromReader(message: Cached, reader: jspb.BinaryReader): Cached;
    }

    export namespace Cached {
        export type AsObject = {
            key: string,
            value?: string,
        }
    }

    export class Prompted extends jspb.Message { 

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): Prompted;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Prompted.AsObject;
        static toObject(includeInstance: boolean, msg: Prompted): Prompted.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Prompted, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Prompted;
        static deserializeBinaryFromReader(message: Prompted, reader: jspb.BinaryReader): Prompted;
    }

    export namespace Prompted {
        export type AsObject = {
            value?: string,
        }
    }

    export class ListConnections extends jspb.Message { 

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): ListConnections.AsObject;
        static toObject(includeInstance: boolean, msg: ListConnections): ListConnections.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: ListConnections, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): ListConnections;
        static deserializeBinaryFromReader(message: ListConnections, reader: jspb.BinaryReader): ListConnections;
    }

    export namespace ListConnections {
        export type AsObject = {
        }
    }


    export enum KindCase {
        KIND_NOT_SET = 0,
        SETUP = 7,
        EXECUTECODE = 1,
        EXECUTEQUERY = 2,
        CANCELEXECUTE = 3,
        CACHED = 4,
        PROMPTED = 5,
        LISTCONNECTIONS = 6,
    }

}

export class FromKernel extends jspb.Message { 

    hasOutput(): boolean;
    clearOutput(): void;
    getOutput(): FromKernel.Output | undefined;
    setOutput(value?: FromKernel.Output): FromKernel;

    hasExecuted(): boolean;
    clearExecuted(): void;
    getExecuted(): FromKernel.Executed | undefined;
    setExecuted(value?: FromKernel.Executed): FromKernel;

    hasReadcache(): boolean;
    clearReadcache(): void;
    getReadcache(): FromKernel.ReadCache | undefined;
    setReadcache(value?: FromKernel.ReadCache): FromKernel;

    hasWritecache(): boolean;
    clearWritecache(): void;
    getWritecache(): FromKernel.WriteCache | undefined;
    setWritecache(value?: FromKernel.WriteCache): FromKernel;

    hasPrompt(): boolean;
    clearPrompt(): void;
    getPrompt(): FromKernel.Prompt | undefined;
    setPrompt(value?: FromKernel.Prompt): FromKernel;

    hasConnections(): boolean;
    clearConnections(): void;
    getConnections(): FromKernel.Connections | undefined;
    setConnections(value?: FromKernel.Connections): FromKernel;

    getKindCase(): FromKernel.KindCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FromKernel.AsObject;
    static toObject(includeInstance: boolean, msg: FromKernel): FromKernel.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FromKernel, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FromKernel;
    static deserializeBinaryFromReader(message: FromKernel, reader: jspb.BinaryReader): FromKernel;
}

export namespace FromKernel {
    export type AsObject = {
        output?: FromKernel.Output.AsObject,
        executed?: FromKernel.Executed.AsObject,
        readcache?: FromKernel.ReadCache.AsObject,
        writecache?: FromKernel.WriteCache.AsObject,
        prompt?: FromKernel.Prompt.AsObject,
        connections?: FromKernel.Connections.AsObject,
    }


    export class Output extends jspb.Message { 
        clearContentList(): void;
        getContentList(): Array<Content>;
        setContentList(value: Array<Content>): Output;
        addContent(value?: Content, index?: number): Content;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Output.AsObject;
        static toObject(includeInstance: boolean, msg: Output): Output.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Output, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Output;
        static deserializeBinaryFromReader(message: Output, reader: jspb.BinaryReader): Output;
    }

    export namespace Output {
        export type AsObject = {
            contentList: Array<Content.AsObject>,
        }
    }

    export class Executed extends jspb.Message { 

        hasDuration(): boolean;
        clearDuration(): void;
        getDuration(): google_protobuf_duration_pb.Duration | undefined;
        setDuration(value?: google_protobuf_duration_pb.Duration): Executed;
        clearErrorsList(): void;
        getErrorsList(): Array<Error>;
        setErrorsList(value: Array<Error>): Executed;
        addErrors(value?: Error, index?: number): Error;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Executed.AsObject;
        static toObject(includeInstance: boolean, msg: Executed): Executed.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Executed, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Executed;
        static deserializeBinaryFromReader(message: Executed, reader: jspb.BinaryReader): Executed;
    }

    export namespace Executed {
        export type AsObject = {
            duration?: google_protobuf_duration_pb.Duration.AsObject,
            errorsList: Array<Error.AsObject>,
        }
    }

    export class ReadCache extends jspb.Message { 
        getKey(): string;
        setKey(value: string): ReadCache;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): ReadCache.AsObject;
        static toObject(includeInstance: boolean, msg: ReadCache): ReadCache.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: ReadCache, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): ReadCache;
        static deserializeBinaryFromReader(message: ReadCache, reader: jspb.BinaryReader): ReadCache;
    }

    export namespace ReadCache {
        export type AsObject = {
            key: string,
        }
    }

    export class WriteCache extends jspb.Message { 
        getKey(): string;
        setKey(value: string): WriteCache;

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): WriteCache;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): WriteCache.AsObject;
        static toObject(includeInstance: boolean, msg: WriteCache): WriteCache.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: WriteCache, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): WriteCache;
        static deserializeBinaryFromReader(message: WriteCache, reader: jspb.BinaryReader): WriteCache;
    }

    export namespace WriteCache {
        export type AsObject = {
            key: string,
            value?: string,
        }
    }

    export class Prompt extends jspb.Message { 

        hasPrompt(): boolean;
        clearPrompt(): void;
        getPrompt(): string | undefined;
        setPrompt(value: string): Prompt;

        hasPlaceholder(): boolean;
        clearPlaceholder(): void;
        getPlaceholder(): string | undefined;
        setPlaceholder(value: string): Prompt;
        getSecret(): boolean;
        setSecret(value: boolean): Prompt;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Prompt.AsObject;
        static toObject(includeInstance: boolean, msg: Prompt): Prompt.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Prompt, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Prompt;
        static deserializeBinaryFromReader(message: Prompt, reader: jspb.BinaryReader): Prompt;
    }

    export namespace Prompt {
        export type AsObject = {
            prompt?: string,
            placeholder?: string,
            secret: boolean,
        }
    }

    export class Connections extends jspb.Message { 
        clearConnectionList(): void;
        getConnectionList(): Array<FromKernel.Connections.Connection>;
        setConnectionList(value: Array<FromKernel.Connections.Connection>): Connections;
        addConnection(value?: FromKernel.Connections.Connection, index?: number): FromKernel.Connections.Connection;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Connections.AsObject;
        static toObject(includeInstance: boolean, msg: Connections): Connections.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Connections, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Connections;
        static deserializeBinaryFromReader(message: Connections, reader: jspb.BinaryReader): Connections;
    }

    export namespace Connections {
        export type AsObject = {
            connectionList: Array<FromKernel.Connections.Connection.AsObject>,
        }


        export class Connection extends jspb.Message { 
            getId(): string;
            setId(value: string): Connection;

            hasProvider(): boolean;
            clearProvider(): void;
            getProvider(): string | undefined;
            setProvider(value: string): Connection;

            serializeBinary(): Uint8Array;
            toObject(includeInstance?: boolean): Connection.AsObject;
            static toObject(includeInstance: boolean, msg: Connection): Connection.AsObject;
            static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
            static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
            static serializeBinaryToWriter(message: Connection, writer: jspb.BinaryWriter): void;
            static deserializeBinary(bytes: Uint8Array): Connection;
            static deserializeBinaryFromReader(message: Connection, reader: jspb.BinaryReader): Connection;
        }

        export namespace Connection {
            export type AsObject = {
                id: string,
                provider?: string,
            }
        }

    }


    export enum KindCase {
        KIND_NOT_SET = 0,
        OUTPUT = 1,
        EXECUTED = 2,
        READCACHE = 3,
        WRITECACHE = 4,
        PROMPT = 5,
        CONNECTIONS = 6,
    }

}

export class Content extends jspb.Message { 
    getType(): string;
    setType(value: string): Content;
    getValue(): Uint8Array | string;
    getValue_asU8(): Uint8Array;
    getValue_asB64(): string;
    setValue(value: Uint8Array | string): Content;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Content.AsObject;
    static toObject(includeInstance: boolean, msg: Content): Content.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Content, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Content;
    static deserializeBinaryFromReader(message: Content, reader: jspb.BinaryReader): Content;
}

export namespace Content {
    export type AsObject = {
        type: string,
        value: Uint8Array | string,
    }
}

export class Error extends jspb.Message { 
    getMessage(): string;
    setMessage(value: string): Error;

    hasStack(): boolean;
    clearStack(): void;
    getStack(): string | undefined;
    setStack(value: string): Error;

    hasPosition(): boolean;
    clearPosition(): void;
    getPosition(): Error.LineAndColumn | undefined;
    setPosition(value?: Error.LineAndColumn): Error;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Error.AsObject;
    static toObject(includeInstance: boolean, msg: Error): Error.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Error, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Error;
    static deserializeBinaryFromReader(message: Error, reader: jspb.BinaryReader): Error;
}

export namespace Error {
    export type AsObject = {
        message: string,
        stack?: string,
        position?: Error.LineAndColumn.AsObject,
    }


    export class LineAndColumn extends jspb.Message { 
        getLine(): number;
        setLine(value: number): LineAndColumn;
        getColumn(): number;
        setColumn(value: number): LineAndColumn;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): LineAndColumn.AsObject;
        static toObject(includeInstance: boolean, msg: LineAndColumn): LineAndColumn.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: LineAndColumn, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): LineAndColumn;
        static deserializeBinaryFromReader(message: LineAndColumn, reader: jspb.BinaryReader): LineAndColumn;
    }

    export namespace LineAndColumn {
        export type AsObject = {
            line: number,
            column: number,
        }
    }

}
